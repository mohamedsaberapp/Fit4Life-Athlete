import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SessionPage } from '../session/session';

/**
 * Generated class for the UpcomingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-upcoming',
  templateUrl: 'upcoming.html',
})
export class UpcomingPage {

  branch: string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.branch='Branchone';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpcomingPage');
  }
  viewSession(){
    this.navCtrl.push(SessionPage);
  }
}

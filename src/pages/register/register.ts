import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  startDate: any="mon";
  expandDateTime:boolean=false;
  expandDay:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  Signup(){
    this.navCtrl.push(TabsPage);
  }
  goLogin(){
    this.navCtrl.setRoot(LoginPage);
  }
  onChange(){
    this.expandDateTime=true;
  }
  showDays(){
    this.expandDay=true;
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { SessionPage } from '../session/session';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  choice: string = 'sessions';
  constructor(public navCtrl: NavController, public navParams: NavParams,private alertCtrl:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  viewSession(){
    this.navCtrl.push(SessionPage);
  }
  addTracker(){
    let alert = this.alertCtrl.create({
      title: 'Add Tracker',
      inputs: [
        {
          name: 'distance',
          placeholder: 'Distance'
        },
        {
          name: 'time',
          placeholder: 'Time'
        },
        {
          name: 'stroke',
          placeholder: 'Stroke'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass:'dark-btn',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add',
          cssClass:'dark-btn',
          handler: data => {
            console.log(data);
          }
        }
      ]
    });
    alert.present();
  }
}

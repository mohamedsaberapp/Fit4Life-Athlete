import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FaqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {
  opened:boolean= false;
  closed:boolean= true;

  openedd:boolean= false;
  closedd:boolean= true;

  openeddd:boolean= false;
  closeddd:boolean= true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqPage');
  }
  open(){
    this.opened=true;
    this.closed=false;
  }
  close(){
    this.opened=false;
    this.closed=true;
  }

  openn(){
    this.openedd=true;
    this.closedd=false;
  }
  closee(){
    this.openedd=false;
    this.closedd=true;
  }

  opennn(){
    this.openeddd=true;
    this.closeddd=false;
  }
  closeee(){
    this.openeddd=false;
    this.closeddd=true;
  }
}

import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  tags: { title: string; }[];
  constructor(public navCtrl: NavController) {
    this.tags=[
      {title:'Sport'},
      {title:'Music'},
      {title:'Theatre'},
      {title:'Gym'},
      {title:'Concerts'},
      {title:'Underground'},
      {title:'Bands'},
    ]
  }

}

import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { UpcomingPage } from '../upcoming/upcoming';
import { ProfilePage } from '../profile/profile';
import { MorePage } from '../more/more';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = UpcomingPage;
  tab3Root = ProfilePage;
  tab4Root = MorePage;

  constructor() {

  }
}
